﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Pages.aspx.cs" Inherits="finalproject.Pages" %>

     <asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <h2><%: Title %></h2>
        
    <!-- interface for adding new pages -->
    <div id="newpagerow">
        <a href="NewPage.aspx">New Page</a>
    </div>
    
      
    <asp:SqlDataSource runat="server"
        id="pages_select"
       
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
    </asp:SqlDataSource>
    <div id="pages_query" class="querybox" runat="server">

    </div>
    <asp:DataGrid id="pages_list"
        runat="server" >
    </asp:DataGrid>

    <div id="debug" runat="server"></div>




</asp:Content>
