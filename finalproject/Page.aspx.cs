﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace finalproject
{
    public partial class Page : System.Web.UI.Page
    {
       
       public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }

        private string page_basequery =
            "SELECT * from pages"; 

       protected void Page_Load(object sender, EventArgs e)
        {

            if (pageid == "" || pageid == null) page_title.InnerHtml = "No page found.";
            else
            {
                //Need to use custom rendering on this one to get the class link
                page_basequery += " WHERE PAGEID = " + pageid;
                //debug.InnerHtml = student_basequery;
                page_select.SelectCommand = page_basequery;
                page_query.InnerHtml = page_basequery;

                
                DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
                //Since we're operating on a primary key we can guarantee we only get 1 row
                DataRowView pagerowview = pageview[0]; //gets the first result which is always the student
                string pagetitle = pageview[0]["pagetitle"].ToString();
                string pagecontent = pageview[0]["pagecontent"].ToString();
                page_title.InnerHtml = pagetitle;
                page_content.InnerHtml = pagecontent;

                

            }
        }

        protected void DelPage(object sender, EventArgs e)
        {
            //Don't want to use a base string for delete 
            //because "DELETE FROM CLASSES"
            //should never be executed, it's very dangerous to delete!
            string delquery = "DELETE FROM pages WHERE pageid=" + pageid;

            del_debug.InnerHtml = delquery;
            del_page.DeleteCommand = delquery;
            del_page.Delete();

            
        }
    }
}