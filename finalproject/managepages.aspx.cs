﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace finalproject
{
    public partial class ManagePages : System.Web.UI.Page
    {
        private string basequery = "SELECT pageid, pagetitle from pages";


        protected void Page_Load(object sender, EventArgs e)
        {
            pages_select.SelectCommand = basequery;
            debug.InnerHtml = basequery;
            //pages_list.DataSource = pages_select;
            pages_list.DataSource = Pages_Manual_Bind(pages_select);
            pages_query.InnerHtml = basequery;
            pages_list.DataBind();
        }

        protected DataView Pages_Manual_Bind(SqlDataSource src)
        {
            DataTable mytbl;
            DataView myview;
            mytbl = ((DataView)src.Select(DataSourceSelectArguments.Empty)).Table;
            foreach (DataRow row in mytbl.Rows)
            {
                //Intercept a specific column rendering
                //add a link of that column info
                row["pagetitle"] =
                    "<a href=\"Page.aspx?pageid="
                    + row["pageid"]
                    + "\">"
                    + row["pagetitle"]
                    + "</a>";

            }
            mytbl.Columns.Remove("pageid");
            myview = mytbl.DefaultView;

            return myview;
        }

    }
}