﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;


namespace finalproject
{
    public partial class PagesUsercontrol : System.Web.UI.UserControl
    {
        //base query
        private string basequery = "SELECT pageid, pagetitle from pages";



        private int selected_id; //Actual field
        public int _selected_id // Accessor
        {
            //standard property accessors
            get { return selected_id; }
            set { selected_id = value; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            //We don't need to add onto the base query.
            //However, if we were imagining a more advanced app,
            //We could imagine showing classes that the particular
            //student is not yet enrolled in!
            //The narrative builds the code!

            Pages_list_pick.SelectCommand = basequery;
            //Did you think that datagrids were the only
            //Thing you could bind to an element?
            //However, we need to render it manually (again).

            //Instead of converting a datasource we instead
            //Manually add items onto the list.
            PagePick_Manual_Bind(Pages_list_pick, "page_pick");

        }

        //take an id (of a dropdownlist) and a datasource.
        //iterate through the datasource and add list items
        //to the dropdownlist.
        void PagePick_Manual_Bind(SqlDataSource src, string ddl_id)
        {
            //The rendering loop here is to print out list items
            //For our asp drop down list.
            String link = "Page.aspx?pageid=";

           
            DataView myview = (DataView)src.Select(DataSourceSelectArguments.Empty);

            foreach (DataRowView row in myview)
            {
                HtmlGenericControl li = new HtmlGenericControl("li");
                page_pick.Controls.Add(li);
                string pid = row["pageid"].ToString();
                link += pid;
                String pagetitle = row["pagetitle"].ToString();
                HtmlGenericControl anchor = new HtmlGenericControl("a");
                anchor.Attributes.Add("href", link);
                anchor.InnerText = pagetitle;
                li.Controls.Add(anchor);
                link = "Page.aspx?pageid=";




            }
        }

      
    }
}