﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace finalproject
{
    public partial class EditPage : System.Web.UI.Page
    { 
        public string pageid
        {
         get { return Request.QueryString["pageid"]; }
        }


        protected void Page_Load(object sender, EventArgs e)
        {
        }
        protected override void OnPreRenderComplete(EventArgs e)
        {
           
         
        
            string basequery = "SELECT * from pages where pageid=" + pageid;
           // debug.InnerHtml = basequery;
            page_select.SelectCommand = basequery;
            DataView pageview = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView pagerowview = pageview[0];

            page_content.Text = pagerowview["pagecontent"].ToString();
            page_title.Text = pagerowview["pagetitle"].ToString();
            


        }




        protected void Edit_Page(object sender, EventArgs e)
        {
            string ptitle = page_title.Text;
            string pcontent = page_content.Text;


            string editquery = "Update pages set pagetitle='" + ptitle + "'," +
                "pagecontent='" + pcontent + "' where pageid=" + pageid;
            debug.InnerHtml = editquery;
            edit_page.UpdateCommand = editquery;
            edit_page.Update();

        }



    }
}

    