﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Data;
using System.Web.UI.WebControls;

namespace finalproject
{
    public partial class DeletePage : System.Web.UI.Page
    {
        public string pageid
        {
            get { return Request.QueryString["pageid"]; }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            page_select.SelectCommand = "select * from pages where pageid = " + pageid;
            DataView page = (DataView)page_select.Select(DataSourceSelectArguments.Empty);
            DataRowView mypagerow = page[0];
            string dbpagetitle = mypagerow["pagetitle"].ToString();
            string dbpagecontent = mypagerow["pagecontent"].ToString();
            
        }
        protected void gotoedit(object sender, EventArgs e)
        {
            Server.Transfer("~/Editpage.aspx");
        }
        protected void Delpage(object sender, EventArgs e)
        {
            string basequery = "DELETE FROM PAGES WHERE PAGEID=" + pageid.ToString();
            del_page.DeleteCommand =basequery;
            del_page.Delete();
        }
    }
}