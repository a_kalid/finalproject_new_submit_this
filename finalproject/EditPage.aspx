﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="EditPage.aspx.cs" Inherits="finalproject.EditPage" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <%-- One SQL command for viewing the existing info --%>
    <asp:SqlDataSource runat="server" id="page_select"
	 ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
        
    </asp:SqlDataSource>

    <%-- One SQL command for editing the student --%>
    <asp:SqlDataSource runat="server" id="edit_page"
        ConnectionString="<%$ ConnectionStrings:final_sql_con %>">
        
    </asp:SqlDataSource>

    
    <div class="inputrow">
        <label>Page Title:</label>
        <asp:textbox id="page_title" runat="server">
        </asp:textbox>
    </div>

    <div class="inputrow">
        <label>Page Content:</label>
        <asp:textbox id="page_content" runat="server">
        </asp:textbox>
    </div>
     <ASP:Button Text="Edit Page" runat="server" OnClick="Edit_Page"/>
    <div id="debug" runat="server"></div>
    test
     
    
    
</asp:Content>



