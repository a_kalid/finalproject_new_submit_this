﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace assignment1a_n01306505
{
    public class Customer
    {
        private string clientLName;
        private string clientFName;
        private string clientPhone;
        private string clientEmail;
        private string clientdropdowncountry;
        private string clientProv;
        private string clientAddress;
        private string clientCity;
        private string clientZip;

        public Customer()
        {
        }

        public string clientLname
        {
            get { return clientLname; }
            set { clientLname = value; }
        }
        public string clientFname
        {
            get { return clientFname; }
            set { clientFname = value; }
        }

        public string clientPhone
        {
            get { return clientPhone; }
            set { clientPhone = value; }
        }

        public string clientEmail
        {
            get { return clientEmail; }
            set { clientEmail = value; }
        }
        public string clientdropdownCountry
        {
            get { return clientdropdownCountry; }
            set { clientdropdownCountry = value; }
        }

        public string clientProv

        {
            get { return clientProv; }
            set { clientProv = value; }
        }

        public string clientAddress
        {
            get { return clientAddress; }
            set { clientAddress = value; }
        }

        public string clientCity
        {
            get { return clientCity; }
            set { clientCity = value; }
        }
        public string clientZip
        {
            get { return clientZip; }
            set { clientZip = value; }
        }
    }
}

    
